package com.ncamc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: hugaoqiang
 * @CreateTime: 2022-08-01 14:45
 */
@SpringBootApplication
@SpringBootConfiguration
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class,args);
    }
}
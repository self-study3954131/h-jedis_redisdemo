package com.ncamc.jedis;

import redis.clients.jedis.Jedis;

import java.util.Random;

/**
 * verifyCode
 * getRedisCode
 * countKey
 * codeKey
 * 1.生成随机6位数字验证码   Random
 * 2.验证码在2分钟内有效     把验证码放到redis里面，设置过期时间120秒
 * 3.判断验证码是否一致       从redis获取验证码和输入的验证码进行比较
 * 4.每个手机每天只能发送3次验证码  incr每次发送之后+1  大于2的时候，提交不能发送
 */
public class Demo {
    public static void main(String[] args) {
//        verifyCode("123");
        getRedisCode("123","789242");
    }

    public static void verifyCode(String phone){
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        String countKey = "verifyCode"+phone+":count";
        String codeKey = "verifyCode"+phone+":code";
        String count = jedis.get(countKey);
        if (count == null){
            jedis.setex(countKey,24*60*60,"1");
        }else if (Integer.parseInt(count) <= 2){
            jedis.incr(countKey);
        }else if (Integer.parseInt(countKey) > 2){
            System.out.println("今天的登录次数已经三次");
            jedis.close();
            return;
        }

        String code = getCode();
        jedis.setex(codeKey,120,code);
        jedis.close();
    }

    public static String getCode(){
        Random random = new Random();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            int rand = random.nextInt(10);
            builder.append(rand);
        }
        return builder.toString();
    }

    public static void getRedisCode(String phone,String code){
        Jedis jedis = new Jedis("127.0.0.1", 6379);
        String codeKey = "verifyCode"+phone+":code";
        String redisCode = jedis.get(codeKey);
        if (redisCode.equals(code)){
            System.out.println("成功");
        }else {
            System.out.println("失败");
        }
        jedis.close();
    }

}
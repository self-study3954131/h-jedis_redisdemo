package com.ncamc.jedis;

import redis.clients.jedis.Jedis;

import java.util.Random;

/**
 * @Author: hugaoqiang
 * @CreateTime: 2022-08-01 16:21
 * 1.生成随机6位数字验证码   Random
 * 2.验证码在2分钟内有效     把验证码放到redis里面，设置过期时间120秒
 * 3.判断验证码是否一致       从redis获取验证码和输入的验证码进行比较
 * 4.每个手机每天只能发送3次验证码  incr每次发送之后+1  大于2的时候，提交不能发送
 */
public class PhoneCode {

    public static void main(String[] args) {
        verifyCode("12387328923");
//        getRedisCode("12387328923","419763");
    }

    //1.生成随机6位数字验证码
    public static String getCode() {
        Random random = new Random();
        StringBuilder code = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            int rand = random.nextInt(10);
            code.append(rand);
        }
        return code.toString();
    }

    //2.每个手机每天只能发送3次验证码,把验证码放到redis里面，设置过期时间
    public static  void verifyCode(String phone){
        //连接jedis
        Jedis jedis = new Jedis("127.0.0.1",6379);
        //拼接key
        //手机发送次数key
        String countKey = "verifyCode"+phone+":count";
        //验证码key
        String codeKey = "verifyCode"+phone+":code";

        //每个手机每天只能发送3次验证码
        String count = jedis.get(countKey);
        if (count == null){
            //没有发送次数，第一次发送
            //设置发送次数是1
            jedis.setex(countKey,24*60*60,"1");
        }else if (Integer.parseInt(count) <= 2){
            jedis.incr(countKey);
        }else if (Integer.parseInt(count) > 2){
            System.out.println("今天的发送次数已经超过三次");
            jedis.close();
            return;
        }

        //发送的验证码放到redis里面
        String  vcode = getCode();
        jedis.setex(codeKey,120,vcode);
        jedis.close();
    }

    //3.验证码校验
    public static void getRedisCode(String phone,String code){
        //连接jedis
        Jedis jedis = new Jedis("127.0.0.1",6379);
        //验证码key
        String codeKey = "verifyCode"+phone+":code";
        String redisCode = jedis.get(codeKey);
        //判断
        if (redisCode.equals(code)){
            System.out.println("成功");
        }else {
            System.out.println("失败");
        }
        jedis.close();
    }

}
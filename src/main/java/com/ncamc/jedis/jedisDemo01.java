package com.ncamc.jedis;

import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.Set;

/**
 * @Author: hugaoqiang
 * @CreateTime: 2022-08-01 14:09
 */
public class jedisDemo01 {

    public static void main(String[] args) {
        Jedis jedis = new Jedis("127.0.0.1",6379);
        //ping
        String value = jedis.ping();
        System.out.println(value);

        jedis.flushDB();
    }

    private Jedis JedisTest() {
        return new Jedis("127.0.0.1",6379);
    }

    /**
     * 使用kye String
     * String 类型，也就是字符串类型，是 Redis 中最简单的存储类型。其 value 是字符串，不过根据字符串的格式不同，又可以分为 3 类：
     *      string：普通字符串
     *      int：整数类型，可以做自增、自减操作
     *      float：浮点类型，可以做自增、自减操作
     * String 类型的常见命令：
     *      set：添加或者修改已经存在的一个 String 类型的键值对
     *      get：根据 key 获取 String 类型的 value
     *      mset：批量添加多个 String 类型的键值对
     *      mget：根据多个 key 获取多个 String 类型的 value
     *      incr：让一个整型的 key 自增 1
     *      incrby：让一个整型的 key 自增并指定步长，例如：incrby num 2 让 num 值自增 2
     *      incrbyfloat：让一个浮点类型的数字自增并指定步长
     *      setnx：添加一个 String 类型的键值对，前提是这个 key 不存在，否则不执行
     *      setex：添加一个 String 类型的键值对，并且指定有效期
     */
    @Test
    public void demo1(){
        String incr = "1";
        String a = "1.11";
        Jedis jedis = JedisTest();
        //单个key-value
        jedis.set("name","lucy");
        jedis.set("name","lisi");
        System.out.println(jedis.get("name"));
        //多个key-value    String
        jedis.mset("k1","v1","k2","v2");
        System.out.println(jedis.mget("k1", "k2"));
        //incr 自增加1
        System.out.println(jedis.incr(incr));
        //incrby 指定步长
        System.out.println(jedis.incrBy(incr, 2));
        //incrbyfloat
        System.out.println(jedis.incrByFloat(a, 1.11));
        //setnx
        jedis.setnx("age","2");
        jedis.setnx("name","zhangsan");
        System.out.println(jedis.get("age"));
        //setex
        jedis.setex("sex",10,"男");
        System.out.println(jedis.get("sex"));

        Set<String> keys = jedis.keys("*");
        for (String key : keys) {
            System.out.println(key);
        }
    }

    /**
     * 使用kye List
     * Redis 中的 List 类型与 Java 中的 LinkedList 类似，可以看做是一个双向链表结构，既可以支持正向检索，也可以支持反向检索。
     * 特征也与 LinkedList 类似：
     *      有序
     *      元素可以重复
     *      插入和删除快
     *      查询速度一般
     * List 类型的常见命令：
     *      lpush key element … ：向列表左侧插入一个或多个元素
     *      lpop key：移除并返回列表左侧的第一个元素，没有则返回 nil
     *      rpush key element … ：向列表右侧插入一个或多个元素
     *      rpop key：移除并返回列表右侧的第一个元素
     *      lrange key start end：返回一段角标范围内的所有元素
     *      blpop 和 blpop：与 lpop 和 rpop 类似，只不过在没有元素时等待指定时间，而不是直接返回 nil
     */
    @Test
    public void demo2(){
        Jedis jedis = JedisTest();
        jedis.lpush("ks1","lucy","mary","jack");
        jedis.lpop("ks1");
        for (String key : jedis.lrange("ks1", 0, -1)) {
            System.out.println(key);
        }

    }

    /**
     * 使用kye Set
     * Redis 的 Set 结构与 Java 中的 HashSet 类似，可以看做是一个 value 为 null 的 HashMap。因为也是一个 hash 表，因此具备与
     * HashSet 类似的特征：
     *      无序
     *      元素不可重复
     *      查找快
     *      支持交集、并集、差集等功能
     * Set 类型的常见命令：
     *      sadd key member … ：向 set 中添加一个或多个元素
     *      srem key member … ：移除 set 中的指定元素
     *      scard key … ：返回 set 中元素的个数
     *      sismember key member：判断一个元素是否存在于 set 中
     *      smembers：获取 set 中的所有元素
     *      sinter key1 key2 … ：求 key1 与 key2 的交集
     *      sdiff key1 key2 … ：求 key1 与 key2 的差集
     *      sunion key1 key2 … ：求 key1 与 key2 的并集
     */
    @Test
    public void demo3(){
        Jedis jedis = JedisTest();
        jedis.sadd("names","lucy","jack");
        for (String name : jedis.smembers("names")) {
            System.out.println(name);
        }
    }

    /**
     * 使用kye Hash
     * Hash 类型，也叫散列，其 value 是一个无序字典，类似于 Java 中的 HashMap 结构。
     * 在存储 Java 对象时，String 结构是将对象序列化为 JSON 字符串后存储，当需要修改对象某个字段时很不方便。
     * Hash 类型的常见命令：
     *      hset key field value：添加或者修改 hash 类型 key 的 field 的值
     *      hget key field：获取一个 hash 类型 key 的 field 的值
     *      hmset：批量添加多个 hash 类型 key 的 field 的值
     *      hmget：批量获取多个 hash 类型 key 的 field 的值
     *      hgetall：获取一个 hash 类型的 key 中的所有的 field 和 value
     *      hkeys：获取一个 hash 类型的 key 中的所有的 field
     *      hvals：获取一个 hash 类型的 key 中的所有的 value
     *      hincrby：让一个 hash 类型的 key 的字段值自增并指定步长
     *      hsetnx：添加一个 hash 类型的 key 的 field 值，前提是这个 field 不存在，否则不执行
     */
    @Test
    public void demo4(){
        Jedis jedis = JedisTest();
        jedis.hset("users","age","20");
        System.out.println(jedis.hget("users", "age"));
    }

    /**
     * 使用kye Zset
     * Redis 的 SortedSet 是一个可排序的 set 集合，与 Java 中的 TreeSet 有些类似，但底层数据结构却差别很大。SortedSet 中的每一个元素都带有一个 score 属性，可以基于 score 属性对元素排序，底层的实现是一个跳表（SkipList）加 hash 表。
     * SortedSet 具备下列特性：
     *      可排序
     *      元素不重复
     *      查询速度快
     *      因为 SortedSet 的可排序特性，经常被用来实现排行榜这样的功能。
     * SortedSet 类型的常见命令有：
     *      zadd key score member：添加一个或多个元素到 SortedSet，如果已经存在则更新其 score 值
     *      zrem key member：删除 SortedSet 中的一个指定元素
     *      zscore key member：获取 SortedSet 中的指定元素的 score 值
     *      zrank key member：获取 SortedSet 中的指定元素的排名
     *      zcard key：获取 SortedSet 中的元素个数
     *      zcount key min max：统计 score 值在给定范围内的所有元素的个数
     *      zincrby key increment member：让 SortedSet 中的指定元素自增，步长为指定的 increment 值
     *      zrange key min max：按照 score 排序后，获取指定排名范围内的元素
     *      zrangebyscore key min max：按照 score 排序后，获取指定 score 范围内的元素
     */
    @Test
    public void demo5(){
        Jedis jedis = JedisTest();
        jedis.zadd("china",100,"shanghai");
        System.out.println(jedis.zrange("china", 0, -1));
    }

}